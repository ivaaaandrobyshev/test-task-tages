up:
	docker-compose -f docker-compose.yaml down -v
	docker-compose -f docker-compose.yaml up -d postgres images-service
	docker-compose -f docker-compose.yaml up --build migrate
	docker-compose -f docker-compose.yaml ps

build:
	docker-compose -f docker-compose.yaml down -v
	docker-compose -f docker-compose.yaml up -d --build postgres images-service
	docker-compose -f docker-compose.yaml up --build migrate
	docker-compose -f docker-compose.yaml ps



down:
	docker-compose -f docker-compose.yaml down -v

generate-images:
	protoc -I api/protos/images \
    		--go_out=api/protos/images \
    		--go_opt=paths=source_relative \
    		--go-grpc_out=api/protos/images \
    		--go-grpc_opt=paths=source_relative \
    		api/protos/images/service.proto

	mkdir -p images/pkg/grpc_stubs/images
	cp -r api/protos/images/* images/pkg/grpc_stubs/images
