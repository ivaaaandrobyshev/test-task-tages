package logging

import (
	"github.com/rs/zerolog"
	"github.com/rs/zerolog/log"
	"os"
)

type Config struct {
	ServiceName string `envconfig:"LOGGING_SERVICE_NAME" required:"true" default:"images"`
	IsDebug     bool   `envconfig:"LOGGING_IS_DEBUG" required:"true" default:"false"`
	LogToFile   bool   `envconfig:"LOGGING_LOG_TO_FILE" required:"false" default:"false"`
}

func New(cfg Config) *zerolog.Logger {

	// Создадим новый logger с полем "service"
	logger := log.With().Str("service", cfg.ServiceName).Logger()

	// Установим уровень логирования на основе IsDebug.
	if cfg.IsDebug {
		logger = logger.Level(zerolog.DebugLevel)
	} else {
		logger = logger.Level(zerolog.InfoLevel)
	}

	// Определим, куда будем писать наши логи - os.STDOUT или в файл, в зависимости от LogToFile.
	if cfg.LogToFile {
		file, err := os.Create("application.log")
		if err != nil {
			logger.Fatal().Err(err).Msg("Failed to create log file")
		}
		logger = logger.Output(file)
	} else {
		logger = logger.Output(zerolog.ConsoleWriter{Out: os.Stdout})
	}

	return &logger
}
