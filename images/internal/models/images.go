package models

import (
	pb "images/pkg/grpc_stubs/images"
	"time"
)

func NewEmptyCreatedImageDTO() *CreateImageDTO {
	return &CreateImageDTO{}
}

func NewEmptyImageFilterDTO() *ImageFilterDTO {
	return &ImageFilterDTO{}
}

// ImageDTO - структура для передачи данных изображения (data transfer object)
type ImageDTO struct {
	Id        int
	Content   []byte
	Filename  string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (i *ImageDTO) ToGrpc() *pb.ImageFileResponse {
	return &pb.ImageFileResponse{
		Id:       int32(i.Id),
		Content:  i.Content,
		Filename: i.Filename,
	}
}

func (i *ImageDTO) ToImageInfoResponse() *pb.ImageInfoResponse {
	return &pb.ImageInfoResponse{
		Id:        int32(i.Id),
		Filename:  i.Filename,
		CreatedAt: i.CreatedAt.Format(time.RFC3339),
		UpdatedAt: i.UpdatedAt.Format(time.RFC3339),
	}

}

// ImageFilterDTO - структура для фильтрации изображений (data transfer object)
type ImageFilterDTO struct {
	Filename string
	DateTo   time.Time
	DateFrom time.Time
}

func (i *ImageFilterDTO) FromGRPC(in *pb.ImageFilter) *ImageFilterDTO {
	dateFrom, _ := time.Parse(time.RFC3339, in.DateFrom)
	dateTo, _ := time.Parse(time.RFC3339, in.DateTo)

	i.Filename = in.Filename
	i.DateFrom = dateFrom
	i.DateTo = dateTo

	return i

}

// CreateImageDTO - структура для создания изображения
type CreateImageDTO struct {
	Content  []byte
	Filename string
}

func (i *CreateImageDTO) FromGrpc(in *pb.CreateImageRequest) *CreateImageDTO {
	i.Content = in.Data
	i.Filename = in.Filename

	return i
}

// ImageInfoDTO - структура для возвращения информации об изображениях
type ImageInfoDTO struct {
	Id        int
	Filename  string
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (i *ImageInfoDTO) ToGrpc() *pb.ImageInfoResponse {
	return &pb.ImageInfoResponse{
		Id:        int32(i.Id),
		Filename:  i.Filename,
		CreatedAt: i.CreatedAt.Format(time.RFC3339),
		UpdatedAt: i.UpdatedAt.Format(time.RFC3339),
	}
}

// ImageDAO - структура для хранения данных изображения (data access object)
type ImageDAO struct {
	Id           int
	Filename     string
	CreatedAt    time.Time
	UpdatedAt    time.Time
	DownloadPath string
}

func (i *ImageDAO) ToDTO() *ImageDTO {
	return &ImageDTO{
		Id:        i.Id,
		Filename:  i.Filename,
		CreatedAt: i.CreatedAt,
		UpdatedAt: i.UpdatedAt,
	}
}
