package service

import (
	"context"
	"images/internal/models"
	"time"
)

type Repository interface {
	GetImageIdByFilename(ctx context.Context, filename string) (int, error)
	InsertImage(ctx context.Context, image *models.ImageDAO) (int, error)

	SetUpdateTime(ctx context.Context, imageId int, time time.Time) error

	GetImages(ctx context.Context, filter *models.ImageFilterDTO) ([]models.ImageDAO, error)
}
