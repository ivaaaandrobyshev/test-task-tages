package service

import (
	"context"
	"crypto/md5"
	"database/sql"
	"errors"
	"fmt"
	"github.com/rs/zerolog"
	"images/internal/api"
	"images/internal/models"
	"images/pkg/osutils"
	"sync"
	"time"
)

type service struct {
	repo      Repository
	imagePath string
	logger    *zerolog.Logger
}

func NewImagesService(
	repo Repository,
	logger *zerolog.Logger,
	imagePath string,
) api.Service {
	return &service{
		repo:      repo,
		logger:    logger,
		imagePath: imagePath,
	}
}

func (s *service) UploadImage(ctx context.Context, dto *models.CreateImageDTO) (int, error) {
	imageID, err := s.repo.GetImageIdByFilename(ctx, dto.Filename)
	if err != nil {
		if !errors.As(err, &sql.ErrNoRows) {
			return 0, fmt.Errorf("[imagesService.UploadImage]: %w", err)
		}
	} else {
		err = s.repo.SetUpdateTime(ctx, imageID, time.Now())
		if err != nil {
			return 0, fmt.Errorf("[imagesService.UploadImage]: %w", err)
		}
		return imageID, nil
	}

	fileHash := md5.Sum([]byte(dto.Filename))

	dirname := fmt.Sprintf("%s/%x/%x", s.imagePath, fileHash[0:3], fileHash[3:5])
	if err = osutils.CreateDir(dirname); err != nil {
		return 0, fmt.Errorf("[imagesService.UploadImage] create dir: %w", err)
	}

	filename := fmt.Sprintf("%s/%x", dirname, fileHash)
	if err = osutils.CreateFile(filename, dto.Content); err != nil {
		return 0, fmt.Errorf("[imagesService.UploadImage] create file: %w", err)
	}

	imageID, err = s.repo.InsertImage(ctx, &models.ImageDAO{
		Filename:     dto.Filename,
		DownloadPath: filename,
		CreatedAt:    time.Now(),
		UpdatedAt:    time.Now(),
	})

	if err != nil {
		return 0, fmt.Errorf("[imagesService.UploadImage] insert image: %w", err)
	}

	return imageID, nil
}

func (s *service) GetImage(ctx context.Context, dto *models.ImageFilterDTO) (<-chan *models.ImageDTO, error) {
	images, err := s.repo.GetImages(ctx, dto)
	if err != nil {
		return nil, fmt.Errorf("[imagesService.GetImage]: %w", err)
	}

	wg := new(sync.WaitGroup)
	imageChan := make(chan *models.ImageDTO)

	for _, imageDAO := range images {
		wg.Add(1)
		go func(imageDAO *models.ImageDAO) {
			defer wg.Done()

			content, err := osutils.SearchFile(imageDAO.DownloadPath)
			if err != nil {
				s.logger.Error().Err(err).Msg("[imagesService.GetImage] search file")
				return
			}

			dto := imageDAO.ToDTO()
			dto.Content = content
			imageChan <- dto

		}(&imageDAO)
	}

	go func() {
		wg.Wait()
		defer close(imageChan)
	}()

	return imageChan, nil
}

func (s *service) GetImageInfo(ctx context.Context, dto *models.ImageFilterDTO) ([]*models.ImageDTO, error) {
	images, err := s.repo.GetImages(ctx, dto)
	if err != nil {
		return nil, fmt.Errorf("[imagesService.GetImageInfo]: %w", err)
	}

	response := make([]*models.ImageDTO, 0, len(images))
	for _, imageDAO := range images {
		response = append(response, imageDAO.ToDTO())
	}

	return response, nil
}
