package api

import (
	"context"
	"images/internal/models"
)

type Service interface {
	UploadImage(ctx context.Context, dto *models.CreateImageDTO) (int, error)

	GetImage(ctx context.Context, dto *models.ImageFilterDTO) (<-chan *models.ImageDTO, error)
	GetImageInfo(ctx context.Context, dto *models.ImageFilterDTO) ([]*models.ImageDTO, error)
}
