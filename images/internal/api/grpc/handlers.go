package grpc

import (
	"context"
	"google.golang.org/grpc/codes"
	"google.golang.org/grpc/status"
	"images/internal/models"
	pb "images/pkg/grpc_stubs/images"
)

var ErrEmptyFileDataOrFilename = status.Error(codes.InvalidArgument, "empty file data or filename")
var ErrInternal = status.Error(codes.Internal, "internal error")

// UploadImage - загрузка изображения
func (s *ImageServer) UploadImage(ctx context.Context, dto *pb.CreateImageRequest) (*pb.ImageIDResponse, error) {
	s.uploadConnLimit <- struct{}{}
	defer func() {
		<-s.uploadConnLimit
	}()
	if dto.Data == nil || dto.Filename == "" {
		s.logger.Error().Msg("empty file data or filename")
		return nil, ErrEmptyFileDataOrFilename
	}
	imageID, err := s.service.UploadImage(ctx, models.NewEmptyCreatedImageDTO().FromGrpc(dto))
	if err != nil {
		s.logger.Error().Err(err).Send()
		return nil, ErrInternal
	}

	return &pb.ImageIDResponse{Id: int32(imageID)}, nil
}

// GetImage - получение изображения файл
func (s *ImageServer) GetImage(filter *pb.ImageFilter, srv pb.ImagesService_GetImageServer) error {
	s.downloadConnLimit <- struct{}{}
	defer func() {
		<-s.downloadConnLimit
	}()

	ctx := srv.Context()
	images, err := s.service.GetImage(ctx, models.NewEmptyImageFilterDTO().FromGRPC(filter))
	if err != nil {
		s.logger.Error().Err(err).Send()
		return ErrInternal
	}

	for image := range images {
		if err = srv.Send(image.ToGrpc()); err != nil {
			s.logger.Error().Err(err).Send()
			return ErrInternal
		}
	}

	return nil
}

// GetImagesInfo - получение информации об изображениях
func (s *ImageServer) GetImagesInfo(ctx context.Context, filer *pb.ImageFilter) (*pb.ImagesInfoResponse, error) {
	s.infoConnLimit <- struct{}{}
	defer func() {
		<-s.infoConnLimit
	}()

	images, err := s.service.GetImageInfo(ctx, models.NewEmptyImageFilterDTO().FromGRPC(filer))
	if err != nil {
		return nil, ErrInternal
	}

	response := make([]*pb.ImageInfoResponse, 0, len(images))

	for _, image := range images {
		response = append(response, image.ToImageInfoResponse())
	}

	return &pb.ImagesInfoResponse{
		Images: response,
	}, nil
}
