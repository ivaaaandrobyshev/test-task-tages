package grpc

import (
	"fmt"
	"github.com/rs/zerolog"
	"google.golang.org/grpc"
	"images/config"
	"images/internal/api"
	pb "images/pkg/grpc_stubs/images"
	"net"
)

type ImageServer struct {
	pb.UnimplementedImagesServiceServer
	cfg     config.GrpcServer
	logger  *zerolog.Logger
	service api.Service

	uploadConnLimit   chan struct{}
	downloadConnLimit chan struct{}
	infoConnLimit     chan struct{}
}

func NewGrpcServer(cfg config.GrpcServer, logger *zerolog.Logger, service api.Service) *ImageServer {
	return &ImageServer{
		cfg:     cfg,
		logger:  logger,
		service: service,

		uploadConnLimit:   make(chan struct{}, cfg.MaxUploadConn),
		downloadConnLimit: make(chan struct{}, cfg.MaxDownloadConnection),
		infoConnLimit:     make(chan struct{}, cfg.MaxInfoConnection),
	}
}

func (s *ImageServer) Start() error {
	srv := grpc.NewServer()
	pb.RegisterImagesServiceServer(srv, s)

	appAddr := fmt.Sprintf("%s:%s", s.cfg.Host, s.cfg.Port)
	lis, err := net.Listen("tcp", appAddr)

	if err != nil {
		return fmt.Errorf("[api.Start] listen: %w", err)
	}

	s.logger.Info().Msgf("running GRPC server at '%s'", appAddr)
	if err = srv.Serve(lis); err != nil {
		return fmt.Errorf("[api.Start] serve: %w", err)
	}

	return nil
}

func (s *ImageServer) Stop() {
	close(s.uploadConnLimit)
	close(s.downloadConnLimit)
	close(s.infoConnLimit)
}
