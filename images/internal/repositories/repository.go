package repositories

import (
	"context"
	"fmt"
	sq "github.com/Masterminds/squirrel"
	"github.com/jackc/pgx/v5/pgxpool"
	"github.com/rs/zerolog"
	"images/internal/models"
	"images/internal/service"
	"time"
)

type imagesRepository struct {
	pool   *pgxpool.Pool
	logger *zerolog.Logger
}

func NewImagesRepository(pool *pgxpool.Pool, logger *zerolog.Logger) service.Repository {
	return &imagesRepository{
		pool:   pool,
		logger: logger,
	}
}

func (r *imagesRepository) GetImageIdByFilename(ctx context.Context, filename string) (int, error) {
	var imageId int
	stmt := `SELECT id FROM images WHERE filename = $1`
	if err := r.pool.QueryRow(ctx, stmt, filename).Scan(&imageId); err != nil {
		return 0, fmt.Errorf("[imagesRepository.GetImageIdByFilename]: %w", err)
	}

	return imageId, nil
}

func (r *imagesRepository) InsertImage(ctx context.Context, image *models.ImageDAO) (int, error) {
	var imageId int
	stmt := `INSERT INTO images (filename, created_at, updated_at, download_path) VALUES ($1, $2, $3, $4) RETURNING id`
	err := r.pool.QueryRow(
		ctx,
		stmt,
		image.Filename, image.CreatedAt, image.UpdatedAt, image.DownloadPath,
	).Scan(&imageId)
	if err != nil {
		return 0, fmt.Errorf("[imagesRepository.InsertImage]: %w", err)
	}

	return imageId, nil
}

func (r *imagesRepository) SetUpdateTime(ctx context.Context, imageId int, time time.Time) error {
	stmt := `UPDATE images SET updated_at = $1 WHERE id = $2`
	_, err := r.pool.Exec(ctx, stmt, time, imageId)
	if err != nil {
		return fmt.Errorf("[imagesRepository.SetUpdateTime]: %w", err)
	}

	return nil
}

func (r *imagesRepository) GetImages(ctx context.Context, filter *models.ImageFilterDTO) ([]models.ImageDAO, error) {
	builder := sq.
		Select("id", "filename", "created_at", "updated_at", "download_path").
		From("images").
		PlaceholderFormat(sq.Dollar)

	if filter.Filename != "" {
		builder = builder.Where(sq.Eq{"filename": filter.Filename})
	}

	if !filter.DateTo.IsZero() {
		builder = builder.Where(sq.LtOrEq{"updated_at": filter.DateTo})
	}

	if !filter.DateFrom.IsZero() {
		builder = builder.Where(sq.GtOrEq{"created_at": filter.DateFrom})
	}

	query, args, err := builder.ToSql()
	if err != nil {
		return nil, fmt.Errorf("[imagesRepository.GetImages]: %w", err)
	}

	rows, err := r.pool.Query(ctx, query, args...)
	if err != nil {
		return nil, fmt.Errorf("[imagesRepository.GetImages]: %w", err)
	}
	defer rows.Close()

	images := make([]models.ImageDAO, 0)
	for rows.Next() {
		var image models.ImageDAO
		if err = rows.Scan(
			&image.Id,
			&image.Filename,
			&image.CreatedAt,
			&image.UpdatedAt,
			&image.DownloadPath,
		); err != nil {
			return nil, fmt.Errorf("[imagesRepository.GetImages]: %w", err)
		}

		images = append(images, image)
	}

	return images, nil
}
