package config

import (
	"github.com/kelseyhightower/envconfig"
	"images/pkg/logging"
	"images/pkg/postgresql"
)

type Config struct {
	Grpc     GrpcServer
	Logging  logging.Config
	Database postgresql.Config

	ImagesPath string `envconfig:"IMAGES_PATH" required:"true" default:"file_storage"`
}

type GrpcServer struct {
	Host                  string `envconfig:"GRPC_HOST" required:"true" default:"0.0.0.0"`
	Port                  string `envconfig:"GRPC_PORT" required:"true" default:"50001"`
	MaxUploadConn         int    `envconfig:"GRPC_MAX_UPLOAD_CONNECTION" required:"true" default:"10"`
	MaxDownloadConnection int    `envconfig:"GRPC_MAX_DOWNLOAD_CONNECTION" required:"true" default:"10"`
	MaxInfoConnection     int    `envconfig:"GRPC_MAX_INFO_CONNECTION" required:"true" default:"100"`
}

type MigrationsConfig struct {
	Postgres postgresql.Config
}

func NewMigrationsFromEnv() *MigrationsConfig {
	cfg := new(MigrationsConfig)
	envconfig.MustProcess("", cfg)

	return cfg
}

func NewFromEnv() *Config {
	cfg := new(Config)
	envconfig.MustProcess("", cfg)

	return cfg
}
