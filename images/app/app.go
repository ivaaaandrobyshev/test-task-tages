package app

import (
	"context"
	"fmt"
	"github.com/rs/zerolog"
	"images/config"
	"images/internal/api/grpc"
	"images/internal/repositories"
	"images/internal/service"
	"images/pkg/logging"
	"images/pkg/postgresql"
	"time"
)

type App struct {
	cfg    *config.Config
	logger *zerolog.Logger
}

func New(cfg *config.Config) *App {
	return &App{
		cfg:    cfg,
		logger: logging.New(cfg.Logging),
	}
}

func (a *App) Run(ctx context.Context) error {
	pool, err := postgresql.NewPgxConn(&a.cfg.Database)
	if err != nil {
		return fmt.Errorf("[app.Run]: %w", err)
	}

	defer pool.Close()

	imagesService := service.NewImagesService(
		repositories.NewImagesRepository(pool, a.logger),
		a.logger,
		a.cfg.ImagesPath,
	)

	grpcServer := grpc.NewGrpcServer(a.cfg.Grpc, a.logger, imagesService)

	errCh := make(chan error, 1)
	defer close(errCh)
	go func() {
		if err = grpcServer.Start(); err != nil {
			errCh <- fmt.Errorf("[app.Run]: %w", err)
		}
	}()

	<-ctx.Done()

	shutdownCtx, cancel := context.WithTimeout(context.Background(), 5*time.Second)
	defer cancel()

	longShutdown := make(chan struct{}, 1)
	defer close(longShutdown)

	go func() {
		grpcServer.Stop()
		longShutdown <- struct{}{}
	}()

	select {
	case <-shutdownCtx.Done():
		a.logger.Error().Err(ctx.Err()).Msg("server shutdown timeout")
	case <-longShutdown:
		a.logger.Info().Msg("server shutdown")
	case err = <-errCh:
		a.logger.Error().Err(err).Send()
		return err
	}

	return nil
}
