package main

import (
	"context"
	_app "images/app"
	"images/config"
	"os/signal"
	"syscall"
)

func main() {
	ctx, stop := signal.NotifyContext(context.Background(), syscall.SIGINT, syscall.SIGTERM)
	defer stop()

	cfg := config.NewFromEnv()

	app := _app.New(cfg)
	if err := app.Run(ctx); err != nil {
		panic(err)
	}

}
